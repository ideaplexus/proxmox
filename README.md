# Terraform - Proxmox

## create-template

This allows you to create a template image from a given cloud image.

Create a `local.tfvars` file with following content:

```
proxmox-host = "your.pve-host.tld"
proxmox-ssh-user = "root"
proxmox-ssh-password = "$uper$ecret"
proxmox-storage = "local-lvm"
proxmox-template-name = "ubuntu-ci"
proxmox-template-vmid = 10000
proxmox-template-url = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
```

If you prefer to use `ssh-agent`, just remove the `proxmox-ssh-password`.

Choose a OS to create an template of:

| OS | URL |
|:---| --- |
| Ubuntu 20.04 LTS | https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img |
| Debian 10 (Buster) | https://cloud.debian.org/images/cloud/buster/daily/latest/debian-10-generic-arm64-daily.qcow2 |
| CentOS  8 | https://cloud.centos.org/altarch/8/x86_64/images/CentOS-8-GenericCloud-8.4.2105-20210603.0.x86_64.qcow2 |

Now init Terraform and apply your settings:

```shell
terraform init
terraform apply --var-file local.tfvars --auto-approve
```

