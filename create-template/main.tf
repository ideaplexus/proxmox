terraform {
  required_providers {
    null = {
      version = "~> 3.1.0"
    }
  }
  required_version = "~> 1.0"
}

resource "null_resource" "install_dependencies" {
  triggers = {
    timestamp = timestamp()
  }
  provisioner "remote-exec" {
    inline = [
      "apt-get -y install libguestfs-tools",
    ]
    connection {
      type     = "ssh"
      host     = var.proxmox-host
      user     = var.proxmox-ssh-user
      agent    = var.proxmox-ssh-password  != "" ? false : true
      password = var.proxmox-ssh-password != "" ? null : var.proxmox-ssh-password
    }
  }
}

resource "null_resource" "create_template" {
  triggers = {
    timestamp = timestamp()
  }
  provisioner "remote-exec" {
    inline = [
      "wget -q -O template.img ${var.proxmox-template-url}",
      "virt-customize -a template.img --install qemu-guest-agent",
      "qm create ${var.proxmox-template-vmid} --name ${var.proxmox-template-name} --memory 2048 --net0 virtio,bridge=vmbr0",
      "qm importdisk ${var.proxmox-template-vmid} template.img ${var.proxmox-storage}",
      "qm set ${var.proxmox-template-vmid} --scsihw virtio-scsi-pci --scsi0 ${var.proxmox-storage}:vm-${var.proxmox-template-vmid}-disk-0",
      "qm set ${var.proxmox-template-vmid} --ide2 ${var.proxmox-storage}:cloudinit",
      "qm set ${var.proxmox-template-vmid} --boot c --bootdisk scsi0",
      "qm set ${var.proxmox-template-vmid} --serial0 socket --vga serial0",
      "qm template ${var.proxmox-template-vmid}",
      "rm -f template.img"
    ]
    connection {
      type = "ssh"
      host = var.proxmox-host
      user = var.proxmox-ssh-user
      agent = true
    }
  }
}
